<?php
namespace App\Http\Controllers\admin;
use App\Entities\Admin\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\admin\StoreUserRequest;
use App\Http\Requests\admin\UpdateUserRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $users = User::paginate(10);
        return view('admin.user.index', [
            'users' => $users,

        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create', [

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {

     $user = new User();
     $user ->firstname = $request->firstname;
     $user ->lastname = $request->lastname;
     $user ->username = $request->username;
     $user ->email = $request->email;
     $user ->password = bcrypt($request->username);

     $user -> created_by =1;
     $user -> update_by = 1;
     $user -> save();

    return redirect()->route('admin.user.show', $user -> id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(user $user)
    {

        return view('admin.user.show',[
            'user' => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {


     $user ->firstname = $request->firstname;
     $user ->lastname = $request->lastname;
     $user ->username = $request->username;
     $user ->email = $request->email;
     $user ->start_date = $request->start_date;
     $user ->save();

    return redirect()->route('admin.user.show', $user->id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
