<?php
use illuminate\support\Str;
use Illuminate\Support\Facades\Route;
/**
 *
 * @param string $route
 * @param string $class
 * @return string
 *
 */

function isRouteActive($route, $class = 'active')
{
    if(Str::contains(Route::currentRouteName(),$route)){
        return $class;
    }

    return null;
}

?>
