<?php
use App\Entities\Admin\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        user::truncate();
        $root = new User();
        $root -> email = 'root@soporte.co';
        $root -> username = 'root';
        $root -> firstname = 'root';
        $root -> password = "password";
        $root -> created_by = 1;
        $root -> update_by = 1;
        $root -> save();

        $user = new User();
        $user-> email = 'corcho@soporte.co';
        $user -> username = 'corcho';
        $user-> firstname = 'corcho';
        $user -> password = bcrypt('corcho');
        $user -> created_by = 1;
        $user -> update_by = 1;
        $user-> save();


        factory(User::class,100)->create([
            'created_by' => 4,
        ]);
    }
}
